import sys

initial = int(sys.argv[-4])
multiplier = int(sys.argv[-3])
size = int(sys.argv[-2])
string = sys.argv[-1]


def multiplicative_string_hash(string):
    string_hash = initial
    hash_multiplier = multiplier

    for character in string:
        string_hash *= hash_multiplier
        string_hash += ord(character)

    return string_hash % size


print(multiplicative_string_hash(string))
