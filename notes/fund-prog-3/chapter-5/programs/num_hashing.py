import sys

size = int(sys.argv[-5])
num = int(sys.argv[-4])
var_1 = int(sys.argv[-3])
var_2 = int(sys.argv[-2])
i = int(sys.argv[-1])

def quadratic():
    h = num % size
    return (h + (var_1 * i) + (var_2 * i * i)) % size

def mod_size():
    return num % size

def h2():
    return var_2 - (num % var_2)

def combined():
    return (mod_size() + (i * h2())) % size

func = combined

print(func())
