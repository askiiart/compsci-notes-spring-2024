-- T-SQL
-- Keywords - UPPERCASE, Tables/Columns/etc - PascalCase, everything else - doesn't matter
-- Comments with double-hyphen
USE AP
GO

SELECT VendorID, VendorName, LEFT(VendorName, 10) VendorShortName,
    VendorAddress1, VendorAddress2, VendorCity
FROM Vendors

-- batch run everything before it
-- GO 2

SELECT *
FROM Terms

SELECT InvoiceID, InvoiceNumber, InvoiceTotal, PaymentTotal, CreditTotal,
    CASE PaymentTotal
		WHEN InvoiceTotal THEN 1
		ELSE 0
	END IsPaidFull
FROM Invoices

-- includes credit
SELECT InvoiceID, InvoiceNumber, InvoiceTotal, PaymentTotal, CreditTotal, InvoiceTotal - CreditTotal PaidTotal,
    CASE PaymentTotal
		WHEN InvoiceTotal - CreditTotal THEN 1
		ELSE 0
	END IsPaidFull
FROM Invoices

-- better
SELECT TOP 500
InvoiceID, InvoiceNumber, InvoiceTotal, PaymentTotal, CreditTotal, InvoiceTotal - CreditTotal PaidTotal,
    CASE WHEN PaymentTotal = InvoiceTotal - CreditTotal THEN 1
		ELSE 0
	END IsPaidFull
FROM Invoices
ORDER BY VendorID, InvoiceDueDate

SELECT TOP 50 InvoiceID, VendorID FROM Invoices
ORDER BY VendorID

-- 2024-01-29

-- paging
-- skip first offset rows, return next fetch rows
SELECT InvoiceID, InvoiceNumber, InvoiceDueDate
FROM Invoices
ORDER BY InvoiceDueDate
OFFSET 110 ROWS
FETCH NEXT 10 ROWS ONLY

-- Filtering
-- just return what you need, instead of filtering in the application
SELECT InvoiceID, VendorID, InvoiceDueDate, InvoiceTotal
FROM Invoices
-- Note that equality is a single equals sign
WHERE InvoiceTotal >= 1000 OR VendorID = 110
ORDER BY VendorID, InvoiceDueDate

SELECT VendorID, VendorName, VendorCity, VendorState
FROM Vendors
-- Note strings use apostrephes, not quotation marks
-- Keep in mind *there is no short-circuit evaluation*, so this will run every single thing and it's messy and slow
--WHERE VendorState = 'CA' OR VendorState = 'AZ' OR VendorState = 'OH' OR VendorState = 'NY'
-- Instead, use IN
WHERE VendorState IN ('CA', 'AZ', 'OH', 'NY')
ORDER BY VendorName

SELECT InvoiceID, InvoiceNumber, InvoiceTotal
FROM Invoices
--WHERE InvoiceTotal >= 100 AND InvoiceTotal <= 500
--less typing and more optimized
--keep in mind it's an inclusive between, not exclusive
WHERE InvoiceTotal BETWEEN 100 AND 500
ORDER BY InvoiceDueDate

-- Haven't paid
-- if either VendorAddress1 or VendorAddress2 are `NULL`, then the concatenation will just be `NULL`
-- so this filters out all the nulls
-- (note that the VendorAddress1 one isn't actually needed because the dataset doesn't contain any that has VendorAddress1 as `NULL` but VendorAddress2 defined)
SELECT VendorID, VendorName, VendorAddress1, VendorAddress2, VendorAddress1 + ' ' + VendorAddress2 FROM Vendors
WHERE VendorAddress1 IS NOT NULL AND VendorAddress2 IS NOT NULL
-- or ISNULL to just return a string instead of NULL
SELECT VendorID, VendorName, VendorAddress1, VendorAddress2, ISNULL(VendorAddress1, '') + ' ' + ISNULL(VendorAddress2, '') FROM Vendors
-- or COALESCE
SELECT VendorID, VendorName, VendorAddress1, VendorAddress2, COALESCE(VendorAddress1, '') + ' ' + COALESCE(VendorAddress2, '') FROM Vendors

-- All vendors with PO Boxes
SELECT VendorID, VendorName, VendorAddress1, VendorAddress2 FROM Vendors
-- '%PO BOX%' is equivalent to '.*PO BOX.*' in regex
WHERE VendorAddress1 LIKE '%PO BOX%' OR VendorAddress2 LIKE '%PO BOX%'

-- Get current date
--SELECT GETDATE(), GET UTCDATE()
-- Get all invoices for January
SELECT InvoiceId, InvoiceNumber, InvoiceDueDate FROM Invoices
WHERE MONTH(InvoiceDueDate) = 1
--WHERE DAY(InvoiceDueDate) IN (30, 31)
-- Get all invoices for 2019
--WHERE YEAR(InvoiceDeDate) = 2019
-- More efficient version, also more flexible, but longer
--WHERE InvoiceDueDate BETWEEN '1/1/2019' AND '12/31/2019'

