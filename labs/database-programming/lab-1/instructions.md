# Lab 1

*UPDATE: Scenario 5 should be `or`, not `and`.*

*UPDATE: Scenario 10 table name is `StateProvinces`.*

This lab has you practice simple queries against tables with filtering and ordering. Write the SQL queries that properly answer each scenario.

For each scenario include the following in your answer:

- The SQL query (properly formatted)
- The number of rows returned by the query

*Note: DO NOT hard code any primary key IDs in your queries.*

This lab uses the `WideWorldImporters` database.

## Scenario 1

Select the `SupplierName`, `WebsiteURL`, `DeliveryAddressLine1`, `DeliveryAddressLine2` and `DeliveryPostalCode` columns from the `Purchasing.Suppliers` table.

Rows: 13

## Scenario 2

Select the `StateProvinceName`, `SalesTerritory`, and `CountryID` from the `Application.StateProvinces` table.
Include only the rows where `SalesTerritory` is `Southeast`.
Order the rows by `StateProvinceName`.

Rows: 12

## Scenario 3

Select the `CustomerID`, `TransactionDate` and `TransactionAmount` from the `Sales.CustomerTransactions` table.
Include only the rows where `TransactionAmount` is less than zero and `TransactionDate` is in the year 2015.
Order the rows by `TransactionDate` descending and then by `TransactionAmount` ascending.

Rows: 8635

## Scenario 4

Select the `CountryName`, `Continent`, `Region`, `Subregion` from the `Application.Countries` table.
Include only the rows where `LatestRecordedPopulation` is greater than 1 million people and `Continent` is not `Oceania`.
Order the rows by `CountryName`, `Continent`, `Region` and then `Subregion`.

Rows: 147

## Scenario 5

Select the `StockItemName`, `RecommendedRetailPrice` and `Tags` from the `Warehouse.StockItems` table.
Include only the rows where `IsChillerStock` is true or `StockItemName` begins with `USB`.
Order the rows by `StockItemName`.
Only return the first 10 rows.

Rows: 10

## Scenario 6

Select the `InvoiceId`, `InvoiceDate` and `CustomerName` from the `Sales.Invoices` and `Sales.Customers` tables.
Include only the invoices from `1/1/2016` to `3/1/2016` and customers that do not have `Toys` in the name.
Order the rows by `CustomerName` and then by `InvoiceDate`.

Rows: 1382

*Note: Return `CustomerName`, not `CustomerId`.

## Scenario 7

Select the `ColorName` columns from the `Warehouse.Colors` table that are not being used in the `Warehouse.StockItems` table.
The `Warehouse.StockItems.ColorId` column contains the `Warehouse.Colors.ColorId` value.
Order by `ColorName`.

Rows: 29

## Scenario 8

Select the `CityName`, `StateProvinceName` and `StateProvinceCode` from the `Application.Cities` table.
Include only the cities that begin with the letter `A` from states that are in the `Southeast` sales territory.
Order the rows by `StateProvinceName` descending and then by `CityName`.

Rows: 376

## Scenario 9

Select the `CustomerId` (as `Id`) and `CustomerName` (as `Name`) columns from the `Sales.Customers` table combined with the `SupplierId` and `SupplierName` columns from the `Purchasing.Suppliers` table.
Include a `Type` column that is set to `Customer` for customer rows and `Supplier` for supplier rows.
Order the resulting data by the type and then by customer/supplier name.

Rows: 676

*Note: The resultset will have only 3 columns (`Type`, `Id` and `Name`).*

## Scenario 10

Select the `CityName`, `StateProvinceName`, `CountryName` and `SalesTerritory` from the `Application.Cities`, `Application.StateProvinces` and `Application.Countries` tables.
Include only the rows that are for the `SalesTerritory` with values of `Southeast` or `Southwest`.
Order the rows by `StateProvinceName` and then by `CityName`.

Rows: 13341
