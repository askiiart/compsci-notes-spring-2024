from Node import Node


class SortedNumberList:
    def __init__(self):
        self.head = None
        self.tail = None

    # Inserts the number into the list in the correct position such that the
    # list remains sorted in ascending order.
    def insert(self, number):
        if self.head == None:
            self.head = Node(number)
            self.tail = self.head
        else:
            current_node = self.head
            new_node = Node(number)
            while True:
                # handles most stuff
                # probably got an error cuz this is confusing
                if current_node != None and current_node.get_data() >= number:
                    new_node.set_next(current_node)
                    new_node.set_previous(current_node.get_previous())

                    if new_node.get_previous() != None:
                        new_node.get_previous().set_next(new_node)
                    else:
                        self.head = new_node

                    current_node.set_previous(new_node)
                    break

                # handles being at the end
                if current_node == None:
                    self.tail.set_next(new_node)
                    new_node.set_previous(self.tail)
                    self.tail = new_node
                    break

                current_node = current_node.get_next()

    # Removes the node with the specified number value from the list. Returns
    # True if the node is found and removed, False otherwise.
    def remove(self, number):
        current_node = self.head
        while True and current_node != None:
            if current_node.get_data() == number:
                next_node = current_node.get_next()
                previous_node = current_node.get_previous()
                if next_node != None:
                    next_node.set_previous(previous_node)
                else:
                    self.tail = previous_node

                if previous_node != None:
                    previous_node.set_next(next_node)
                else:
                    self.head = next_node
                return True

            current_node = current_node.get_next()

        return False
