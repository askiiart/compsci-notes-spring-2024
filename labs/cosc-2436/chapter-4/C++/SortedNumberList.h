#ifndef SORTEDNUMBERLIST_H
#define SORTEDNUMBERLIST_H
#include "Node.h"

class SortedNumberList {
  private:
    // Optional: Add any desired private functions here

    /*
    These traverse the list up/down from startingNode and updates the head/tail

    Less efficient than just doing it in the functions based off the stuff there, but this is a bit easier on my brain
        than dealing with edge cases there. Maybe the compiler can optimize it a bit, idk, I don't feel like digging
        into the assembly (well, not right now)

    Note: These used to be one function, UpdateHeadAndTail(), but I've split UpdateHead() and UpdateTail() into two
        different functions in order to minimize the performance hit.
    */
    void UpdateHead(Node *startingNode) {
        Node *currentNode = startingNode;
        while (currentNode->GetPrevious() != nullptr) {
            currentNode = currentNode->GetPrevious();
        }
        head = currentNode;
    }

    void UpdateTail(Node *startingNode) {
        Node *currentNode = startingNode;
        while (currentNode->GetNext() != nullptr) {
            currentNode = currentNode->GetNext();
        }
        tail = currentNode;
    }

  public:
    Node *head;
    Node *tail;

    SortedNumberList() {
        head = nullptr;
        tail = nullptr;
    }

    // Inserts the number into the list in the correct position such that the
    // list remains sorted in ascending order.
    void Insert(double number) {
        if (head == nullptr) { // handle being a new list with no data in it
                               // if (false) {
            head = new Node(number);
            tail = head;
        }
        else { // actually do anything interesting
            Node *currentNode = head;
            while (true) {
                // needs debugging, if less than head, replaces head, if more, throws SIGSEGV (Address boundary error)
                if (currentNode->GetData() <= number &&
                    (currentNode->GetNext() == nullptr || currentNode->GetNext()->GetData() <= number)) {
                    Node *newNode = new Node(number, currentNode->GetNext(), currentNode);
                    if (newNode->GetNext() != nullptr) {
                        currentNode->SetNext(newNode);
                    }
                    else {
                        head = newNode;
                    }

                    if (newNode->GetNext() != nullptr) {
                        newNode->GetNext()->SetPrevious(newNode);
                    }
                    else {
                        tail = newNode;
                    }
                    // I split these into 2 different functions so they *could* be more efficient,
                    // not so they *would* be more efficient :P
                    // UpdateHead(newNode);
                    // UpdateTail(newNode);
                    break;
                }
                currentNode = currentNode->GetNext();
            }
        }
    }

    // Removes the node with the specified number value from the list. Returns
    // true if the node is found and removed, false otherwise.
    bool Remove(double number) {
        // deal with having an empty list
        if (head == nullptr) {
            return false;
        }
        else {
            Node *currentNode = head;
            while (currentNode != nullptr) {
                if (currentNode->GetData() == number) {
                    // Change around the adjacent nodes to point to each other
                    // Gotta deal with being the start or ending node
                    // (next or prev being nullptr), though
                    if (currentNode->GetPrevious() != nullptr) {
                        currentNode->GetPrevious()->SetNext(currentNode->GetNext());
                    }
                    if (currentNode->GetNext() != nullptr) {
                        currentNode->GetNext()->SetPrevious(currentNode->GetPrevious());
                    }
                    UpdateHead(currentNode);
                    UpdateTail(currentNode);
                    return true;
                }
                currentNode = currentNode->GetNext();
            }
            return false;
        }
    }
};

#endif
